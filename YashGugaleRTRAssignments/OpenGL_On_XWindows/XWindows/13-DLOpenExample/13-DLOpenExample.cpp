//DLOpen Example:

//Headers:
#include <stdlib.h>
#include <stdio.h>
#include <dlfcn.h>

int main()
{
	//code:
	
	void *handle;
	typedef double (*cosine_fn_ptr)(double);//Typecast to return function pointer
	cosine_fn_ptr cosine;	//Create a function pointer
	char *error;
	
	handle = dlopen("/lib/x86_64-linux-gnu/libm.so.6", RTLD_LAZY);	//Create a handle to the library
	if(!handle)	//If error, print on standard terminal
	{
		fputs(dlerror(), stderr);
		exit(1);
	}
	
	cosine = (cosine_fn_ptr)dlsym(handle, "cos");
	if((error = dlerror()) != NULL)
	{
		fputs(error, stderr);
		exit(1);
	}
	
	printf("%f\n", (*cosine)(0.0));	//Use the cos function on 0.0
	dlclose(handle);	//Close the library
}
