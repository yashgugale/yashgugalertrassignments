//* 02-Event Handling */

// global variables:
var canvas = null;
var context = null;
	
// onload function:
function main()
{
	// Get <canvas> element:
	canvas = document.getElementById("AMC");
	if(!canvas)
		console.log("Failure in obtaining Canvas\n");
	else
		console.log("Canvas is successfully obtained\n");
	
	// Print canvas width and height on the console:
	console.log("Canvas width : " + canvas.width + "\nCanvas height : " + canvas.height);
	
	// Get 2D context:
	context = canvas.getContext("2d");
	if(!context)
		console.log("Failure in obtaining 2D context\n");
	else
		console.log("2D context is successfully obtained\n");
	
	// Fill the canvas with black color:
	context.fillStyle="black";				// "#000000"
	context.fillRect(0, 0, canvas.width, canvas.height);
	
	// Center the text:
	context.textAlign="center";				// Center horizontally
	context.textBaseline="middle";			// Center vertically
	
	// Text:
	var str = "Hello world from WebGL!";
	
	// Text font:
	context.font="48px sans-serif";
	
	// Text color:
	context.fillStyle="white";				// "#FFFFFF"
	
	// Display the text in the center:
	context.fillText(str, canvas.width/2, canvas.height/2);
	
	// Register keyboard's keydown and mouse's click event handlers:
	window.addEventListener("keydown", keyDown, false);
	window.addEventListener("click", mouseDown, false);
}

function keyDown(event)
{
	// Code:
	alert("A key is pressed");
}

function mouseDown(event)
{
	// COde:
	alert("Mouse is clicked");
}



