// 05-OpenGL Triangle ortho

// Global Variables:
var canvas=null;			// Canvas element
var gl=null;				// WebGL context for WebGL functionality
var bFullscreen=false;		// Toggle Fullscreen
var canvas_original_width;	// Canvas width
var canvas_original_height;	// Canvas height

const WebGLMacros = // When whole 'WebGLMacros' is cont, all inside it are automatically 'const'
{
	YSG_ATTRIBUTE_POSITION:0,
	YSG_ATTRIBUTE_COLOR:1,
	YSG_ATTRIBUTE_NORMAL:2,
	YSG_ATTRIBUTE_TEXTURE:3
};

// Shader objects:
var vertexShaderObject;
var fragmentShaderObject;
var shaderProgramObject;

// VAO and VBO's:
var vao;
var vbo;
var mvpUniform;						// MVP Uniform variable

var orthographicProjectionMatrix;	// Orthographic projection matrix

// To start animation:
// To have requestAnimationFrame() to be called, "cross-browser" compatible:
var requestAnimationFrame = 
	window.requestAnimationFrame ||				// Common animation
	window.webkitRequestAnimationFrame ||		// Mac OS animation
	window.mozRequestAnimationFrame ||			// Mozilla animation
	window.oRequestAnimationFrame ||			// Opera animation
	window.msRequestAnimationFrame;				// Microsoft animation

// To stop animation:
// To have cancelAnimationFrame() to be called, "cross-browser' compatible:
var cancelAnimationFrame = 
	window.cancelAnimationFrame ||														// All browsers
	window.webkitCancelRequestAnimationFrame || window.webkitCancelAnimationFrame ||	// Safari
	window.mozCancelRequestAnimationFrame || window.mozCancelAnimationFrame ||			// Mozilla
	window.oCancelRequestAnimationFrame || window.oCancelAnimationFrame ||				// Opera
	window.msCancelRequestAnimationFrame || window.msCancelAnimationFrame;				// Internet Explorer
	
// onload funtion:
function main()
{
	// Get <canvas> element:
	canvas = document.getElementById("AMC");
	if(!canvas)
		console.log("Failed to obtain canvas\n");
	else
		console.log("canvas successfully obtained\n");
	
	// Save the width and height to be used later:
	canvas_original_width = canvas.width;
	canvas_original_height = canvas.height;
	
	// Resiter event handler's:
	window.addEventListener("keydown", keyDown, false);
	window.addEventListener("click", mouseDown, false);
	window.addEventListener("resize", resize, false);
	
	// Initialize WebGL:
	init();
	
	// Start drawing here as a warm-up call:
	resize();
	// As no paint/repaint functionality in javascript, we need to do initilize drawing explicitly as 
	// resize does not do so
	draw();
}

// Function to toggle fullscreen:
function toggleFullScreen()
{
	// Code:
	var fullscreen_element = 
		document.fullscreenElement ||			// For common browsers
		document.webkitFullscreenElement ||		// For Safari
		document.mozFullScreenElement ||		// For Mozilla
		document.msFullscreenElement ||			// For Internet Explorer
		null;
	
	// If not fullscreen:
	if(fullscreen_element == null)
	{
		if(canvas.requestFullscreen)
			canvas.requestFullscreen();
		else if(canvas.webkitRequestFullscreen)
			canvas.webkitRequestFullscreen();
		else if(canvas.mozRequestFullScreen)
			canvas.mozRequestFullScreen();
		else if(canvas.msRequestFullscreen)
			canvas.msRequestFullscreen();
		
		bFullscreen = true;						// Set the fullscreen variable
	}
	
	// If already fullscreen:
	else
	{
		if(document.exitFullscreen)
			document.exitFullscreen();
		else if(document.webkitExitFullscreen)
			document.webkitExitFullscreen();
		else if(document.mozCancelFullScreen)
			document.mozCancelFullScreen();
		else if(document.msExitFullscreen)
			document.msExitFullscreen();
		
		bFullscreen = false;					// Set the fullscreen variable
	}
}

// Init function:
function init()
{
	// Code:
	// Get WebGL 2.0 context:
	gl = canvas.getContext("webgl2");
	if(!gl)			// Failure to get context
	{
		console.log("Failure in obtaining WebGL 2.0 rendering context");
		return;
	}
	
	gl.viewportWidth = canvas.width;
	gl.viewportHeight = canvas.height;
	
	// Vertex shader:
	vertexShaderObject = gl.createShader(gl.VERTEX_SHADER);
	
	// Vertex Shader source code:
	var vertexShaderSourceCode = 
	"#version 300 es"+
	"\n"+
	"in vec4 vPosition;"+
	"uniform mat4 u_mvp_matrix;"+
	"void main(void)"+
	"{"+
	"gl_Position = u_mvp_matrix * vPosition;"+
	"}";
	
	// Assign the source and compile:
	gl.shaderSource(vertexShaderObject, vertexShaderSourceCode);
	gl.compileShader(vertexShaderObject);
	
	// Error checking:
	if(gl.getShaderParameter(vertexShaderObject, gl.COMPILE_STATUS) == false)
	{
		var error = gl.getShaderInfoLog(vertexShaderObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}
	
	// Fragment shader:
	fragmentShaderObject = gl.createShader(gl.FRAGMENT_SHADER);
	
	// Fragment Shader Source Code:
	var fragmentShaderSourceCode =
	"#version 300 es"+
	"\n"+
	"precision highp float;"+
	"out vec4 FragColor;"+
	"void main(void)"+
	"{"+
	"FragColor = vec4(1.0, 1.0, 1.0, 1.0);"+
	"}";
	
	// Assign the source and compile:
	gl.shaderSource(fragmentShaderObject, fragmentShaderSourceCode);
	gl.compileShader(fragmentShaderObject);
	
	// Error checking:
	if(gl.getShaderParameter(fragmentShaderObject, gl.COMPILE_STATUS) == false)
	{
		var error = gl.getShaderInfoLog(fragmentShaderObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}
	
	// Shader Program:
	// Create a shader program object and attach the two shaders:
	shaderProgramObject = gl.createProgram();
	gl.attachShader(shaderProgramObject, vertexShaderObject);
	gl.attachShader(shaderProgramObject, fragmentShaderObject);
	
	// pre-link binding of shader program object with vertex shader attributes:
	gl.bindAttribLocation(shaderProgramObject, WebGLMacros.YSG_ATTRIBUTE_POSITION, "vPosition");
	
	// Linking:
	gl.linkProgram(shaderProgramObject);
	
	// Error checking:
	if(!gl.getProgramParameter(shaderProgramObject, gl.LINK_STATUS))
	{
		var error = gl.getProgramInfoLog(shaderProgramObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}
	
	// Get MVP uniform locations:
	mvpUniform = gl.getUniformLocation(shaderProgramObject, "u_mvp_matrix");
	
	// Vertex, Color, Normals, Shader attribs, vao, vbo initialization:
	var triangleVertices = new Float32Array([
											0.0, 50.0, 0.0, 	// Apex
											-50.0, -50.0, 0.0, 	// Left-bottom
											50.0, -50.0, 0.0	// Right-bottom
											]);
	
	// Bind vao:
	vao = gl.createVertexArray();
	gl.bindVertexArray(vao);
		
	// Bind vbo:
	vbo = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vbo);
	gl.bufferData(gl.ARRAY_BUFFER, triangleVertices, gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.YSG_ATTRIBUTE_POSITION,
							3,		// For x, y, z co-ordinates of our triangleVertices
							gl.FLOAT,
							false, 0, 0);
	gl.enableVertexAttribArray(WebGLMacros.YSG_ATTRIBUTE_POSITION);
	
	// Release the buffers and vao:
	gl.bindBuffer(gl.ARRAY_BUFFER, null);
	gl.bindVertexArray(null);
	
	// Set clearColor:
	gl.clearColor(0.0, 0.0, 1.0, 1.0);
	
	// Initialize the projection matrix:
	orthographicProjectionMatrix = mat4.create();
}

// Resize function:
function resize()
{
	// Code:
	if(bFullscreen == true)
	{
		canvas.width = window.innerWidth;
		canvas.height = window.innerHeight;
	}
	else
	{
		canvas.width = canvas_original_width;
		canvas.height = canvas_original_height;
	}
	
	// Set the viewport to match the canvas:
	gl.viewport(0, 0, canvas.width, canvas.height);
	
	// Orthographic project => left, right, bottom, top, near, far:
    if (canvas.width <= canvas.height)
        mat4.ortho(orthographicProjectionMatrix, -100.0, 100.0, (-100.0 * (canvas.height / canvas.width)), (100.0 * (canvas.height / canvas.width)), -100.0, 100.0);
    else
        mat4.ortho(orthographicProjectionMatrix, (-100.0 * (canvas.width / canvas.height)), (100.0 * (canvas.width / canvas.height)), -100.0, 100.0, -100.0, 100.0);

}

// Display/Drawing function:
function draw()
{
	// Code:
	gl.clear(gl.COLOR_BUFFER_BIT);
	
	gl.useProgram(shaderProgramObject);
	
	// Create and set matrices to identity:
	var modelViewMatrix = mat4.create();
	var modelViewProjectionMatrix = mat4.create();
	
	mat4.multiply(modelViewProjectionMatrix, orthographicProjectionMatrix, modelViewMatrix);
	gl.uniformMatrix4fv(mvpUniform, false, modelViewProjectionMatrix);
	
	gl.bindVertexArray(vao);
	gl.drawArrays(gl.TRIANGLES, 0, 3);
	gl.bindVertexArray(null);
	gl.useProgram(null);
	
	// Animation loop/Game loop:
	// We use a function pointer to call the function and pass the same function name as a parameter so
	// that the function is called in a loop and thus forming the game loop:
	requestAnimationFrame(draw, canvas);
}

function uninitialize()
{
	// Code:
	if(vao)
	{
		gl.deleteVertexArray(vao);
		vao = null;
	}
	
	if(vbo)
	{
		gl.deleteBuffer(vbo);
		vbo = null;
	}
	
	if(shaderProgramObject)
	{
		if(vertexShaderObject)
		{
			gl.detachShader(shaderProgramObject, vertexShaderObject);
			gl.deleteShader(vertexShaderObject);
			vertexShaderObject = null;
		}
		
		if(fragmentShaderObject)
		{
			gl.detachShader(shaderProgramObject, fragmentShaderObject);
			gl.deleteShader(fragmentShaderObject);
			fragmentShaderObject = null;
		}
		
		gl.deleteProgram(shaderProgramObject);
		shaderProgramObject = null;
	}
}
		
// Event Handling functions:
function keyDown()
{
	// Code:
	switch(event.keyCode)
	{
		case 27:		// Escape
			// uninitialize:
			uninitialize();
			window.close();		// Close our application's tab (may not work in Firefox)
			break;
			
		case 70:		// For 'F' or 'f':
			toggleFullScreen();
			break;
	}
}

function mouseDown()
{
	// Code:
}
