// 04-OpenGL blue screen

// Global Variables:
var canvas=null;			// Canvas element
var gl=null;				// WebGL context for WebGL functionality
var bFullscreen=false;		// Toggle Fullscreen
var canvas_original_width;	// Canvas width
var canvas_original_height;	// Canvas height

// To start animation:
// To have requestAnimationFrame() to be called, "cross-browser" compatible:
var requestAnimationFrame = 
	window.requestAnimationFrame ||				// Common animation
	window.webkitRequestAnimationFrame ||		// Mac OS animation
	window.mozRequestAnimationFrame ||			// Mozilla animation
	window.oRequestAnimationFrame ||			// Opera animation
	window.msRequestAnimationFrame;				// Microsoft animation

// To stop animation:
// To have cancelAnimationFrame() to be called, "cross-browser' compatible:
var cancelAnimationFrame = 
	window.cancelAnimationFrame ||														// All browsers
	window.webkitCancelRequestAnimationFrame || window.webkitCancelAnimationFrame ||	// Safari
	window.mozCancelRequestAnimationFrame || window.mozCancelAnimationFrame ||			// Mozilla
	window.oCancelRequestAnimationFrame || window.oCancelAnimationFrame ||				// Opera
	window.msCancelRequestAnimationFrame || window.msCancelAnimationFrame;				// Internet Explorer
	
// onload funtion:
function main()
{
	// Get <canvas> element:
	canvas = document.getElementById("AMC");
	if(!canvas)
		console.log("Failed to obtain canvas\n");
	else
		console.log("canvas successfully obtained\n");
	
	// Save the width and height to be used later:
	canvas_original_width = canvas.width;
	canvas_original_height = canvas.height;
	
	// Resiter event handler's:
	window.addEventListener("keydown", keyDown, false);
	window.addEventListener("click", mouseDown, false);
	window.addEventListener("resize", resize, false);
	
	// Initialize WebGL:
	init();
	
	// Start drawing here as a warm-up call:
	resize();
	// As no paint/repaint functionality in javascript, we need to do initilize drawing explicitly as 
	// resize does not do so
	draw();
}

// Function to toggle fullscreen:
function toggleFullScreen()
{
	// Code:
	var fullscreen_element = 
		document.fullscreenElement ||			// For common browsers
		document.webkitFullscreenElement ||		// For Safari
		document.mozFullScreenElement ||		// For Mozilla
		document.msFullscreenElement ||			// For Internet Explorer
		null;
	
	// If not fullscreen:
	if(fullscreen_element == null)
	{
		if(canvas.requestFullscreen)
			canvas.requestFullscreen();
		else if(canvas.webkitRequestFullscreen)
			canvas.webkitRequestFullscreen();
		else if(canvas.mozRequestFullScreen)
			canvas.mozRequestFullScreen();
		else if(canvas.msRequestFullscreen)
			canvas.msRequestFullscreen();
		
		bFullscreen = true;						// Set the fullscreen variable
	}
	
	// If already fullscreen:
	else
	{
		if(document.exitFullscreen)
			document.exitFullscreen();
		else if(document.webkitExitFullscreen)
			document.webkitExitFullscreen();
		else if(document.mozCancelFullScreen)
			document.mozCancelFullScreen();
		else if(document.msExitFullscreen)
			document.msExitFullscreen();
		
		bFullscreen = false;					// Set the fullscreen variable
	}
}

// Init function:
function init()
{
	// Code:
	// Get WebGL 2.0 context:
	gl = canvas.getContext("webgl2");
	if(!gl)			// Failure to get context
	{
		console.log("Failure in obtaining WebGL 2.0 rendering context");
		return;
	}
	
	gl.viewportWidth = canvas.width;
	gl.viewportHeight = canvas.height;
	
	// Set clearcolor:
	gl.clearColor(0.0, 0.0, 1.0, 1.0);
}

// Resize function:
function resize()
{
	// Code:
	if(bFullscreen == true)
	{
		canvas.width = window.innerWidth;
		canvas.height = window.innerHeight;
	}
	else
	{
		canvas.width = canvas_original_width;
		canvas.height = canvas_original_height;
	}
	
	// Set the viewport to match the canvas:
	gl.viewport(0, 0, canvas.width, canvas.height);
}

// Display/Drawing function:
function draw()
{
	// Code:
	gl.clear(gl.COLOR_BUFFER_BIT);
	
	// Animation loop/Game loop:
	// We use a function pointer to call the function and pass the same function name as a parameter so
	// that the function is called in a loop and thus forming the game loop:
	requestAnimationFrame(draw, canvas);
}

// Event Handling functions:
function keyDown()
{
	// Code:
	switch(event.keyCode)
	{
		case 70:		// For 'F' or 'f':
			toggleFullScreen();
			break;
	}
}

function mouseDown()
{
	// Code:
}
