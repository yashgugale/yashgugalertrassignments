//* 03-Fullscreen */

// global variables:
var canvas = null;
var context = null;
	
// onload function:
function main()
{
	// Get <canvas> element:
	canvas = document.getElementById("AMC");
	if(!canvas)
		console.log("Failure in obtaining Canvas\n");
	else
		console.log("Canvas is successfully obtained\n");
	
	// Print canvas width and height on the console:
	console.log("Canvas width : " + canvas.width + "\nCanvas height : " + canvas.height);
	
	// Get 2D context:
	context = canvas.getContext("2d");
	if(!context)
		console.log("Failure in obtaining 2D context\n");
	else
		console.log("2D context is successfully obtained\n");
	
	// Fill the canvas with black color:
	context.fillStyle="black";				// "#000000"
	context.fillRect(0, 0, canvas.width, canvas.height);
	
	// Draw text:
	drawText("Hello world from WebGL!");
	
	
	// Register keyboard's keydown and mouse's click event handlers:
	window.addEventListener("keydown", keyDown, false);
	window.addEventListener("click", mouseDown, false);
}

function drawText(text)
{
	// Code:
	// Center the text:
	context.textAlign="center";				// Center horizontally
	context.textBaseline="middle";			// Center vertically
	
	// Text font:
	context.font="48px sans-serif";
	
	// Text color:
	context.fillStyle="white";				// "#FFFFFF"
	
	// Display the text in the center:
	context.fillText(text, canvas.width/2, canvas.height/2);
	
}

function toggleFullScreen()
{
	var fullscreen_element = 
								document.fullscreenElement ||
								document.webkitFullscreenElement ||
								document.mozFullScreenElement ||
								document.msFullscreenElement ||
								null;
								
	// If no fullscreen:
	if(fullscreen_element == null)
	{
		if(canvas.requestFullscreen)
			canvas.requestFullscreen();
		else if(canvas.mozRequestFullScreen)
			canvas.mozRequestFullScreen();
		else if(canvas.webkitRequestFullscreen)
			canvas.webkitRequestFullscreen();
		else if(canvas.msRequestFullscreen)
			canvas.msRequestFullscreen();
	}
	// Else, if already full screen:
	else
	{
		if(document.exitFullscreen)
			document.exitFullscreen();
		else if(document.mozCancelFullScreen)
			document.mozCancelFullScreen();
		else if(document.webkitExitFullscreen)
			document.webkitExitFullscreen();
		else if(document.msExitFullscreen)
			document.msExitFullscreen();
	}
}

// Keyboard key press handling function:
function keyDown(event)
{
	// Code:
	switch(event.keyCode)
	{
		case 70:	// for 'F' or 'f':
			toggleFullScreen();
			
			// Repaint:
			drawText("Hello world from WebGL!");
			break;
	}
}

// Mouse button press handling function:
function mouseDown(event)
{
	// COde:
}



