// 10-OpenGL pyramid and cube rotation:

// Global Variables:
var canvas=null;			// Canvas element
var gl=null;				// WebGL context for WebGL functionality
var bFullscreen=false;		// Toggle Fullscreen
var canvas_original_width;	// Canvas width
var canvas_original_height;	// Canvas height

const WebGLMacros = // When whole 'WebGLMacros' is const, all inside it are automatically 'const'
{
	YSG_ATTRIBUTE_POSITION:0,
	YSG_ATTRIBUTE_COLOR:1,
	YSG_ATTRIBUTE_NORMAL:2,
	YSG_ATTRIBUTE_TEXTURE:3
};

// Shader objects:
var vertexShaderObject;
var fragmentShaderObject;
var shaderProgramObject;

// VAO and VBO's:
var vao_Pyramid;
var vao_Cube;
var vbo_Pyramid_Position;
var vbo_Pyramid_Color;
var vbo_Cube_Position;
var vbo_Cube_Color;			
var mvpUniform;						// MVP Uniform variable

var anglePyramid = 0.0;				// Rotation for pyramid
var angleCube = 0.0;				// Rotation for cube

var perspectiveProjectionMatrix;	// Perspective projection matrix

// To start animation:
// To have requestAnimationFrame() to be called, "cross-browser" compatible:
var requestAnimationFrame = 
	window.requestAnimationFrame ||				// Common animation
	window.webkitRequestAnimationFrame ||		// Mac OS animation
	window.mozRequestAnimationFrame ||			// Mozilla animation
	window.oRequestAnimationFrame ||			// Opera animation
	window.msRequestAnimationFrame;				// Microsoft animation

// To stop animation:
// To have cancelAnimationFrame() to be called, "cross-browser' compatible:
var cancelAnimationFrame = 
	window.cancelAnimationFrame ||														// All browsers
	window.webkitCancelRequestAnimationFrame || window.webkitCancelAnimationFrame ||	// Safari
	window.mozCancelRequestAnimationFrame || window.mozCancelAnimationFrame ||			// Mozilla
	window.oCancelRequestAnimationFrame || window.oCancelAnimationFrame ||				// Opera
	window.msCancelRequestAnimationFrame || window.msCancelAnimationFrame;				// Internet Explorer
	
// onload funtion:
function main()
{
	// Get <canvas> element:
	canvas = document.getElementById("AMC");
	if(!canvas)
		console.log("Failed to obtain canvas\n");
	else
		console.log("canvas successfully obtained\n");
	
	// Save the width and height to be used later:
	canvas_original_width = canvas.width;
	canvas_original_height = canvas.height;
	
	// Resiter event handler's:
	window.addEventListener("keydown", keyDown, false);
	window.addEventListener("click", mouseDown, false);
	window.addEventListener("resize", resize, false);
	
	// Initialize WebGL:
	init();
	
	// Start drawing here as a warm-up call:
	resize();
	// As no paint/repaint functionality in javascript, we need to do initilize drawing explicitly as 
	// resize does not do so
	draw();
}

// Function to toggle fullscreen:
function toggleFullScreen()
{
	// Code:
	var fullscreen_element = 
		document.fullscreenElement ||			// For common browsers
		document.webkitFullscreenElement ||		// For Safari
		document.mozFullScreenElement ||		// For Mozilla
		document.msFullscreenElement ||			// For Internet Explorer
		null;
	
	// If not fullscreen:
	if(fullscreen_element == null)
	{
		if(canvas.requestFullscreen)
			canvas.requestFullscreen();
		else if(canvas.webkitRequestFullscreen)
			canvas.webkitRequestFullscreen();
		else if(canvas.mozRequestFullScreen)
			canvas.mozRequestFullScreen();
		else if(canvas.msRequestFullscreen)
			canvas.msRequestFullscreen();
		
		bFullscreen = true;						// Set the fullscreen variable
	}
	
	// If already fullscreen:
	else
	{
		if(document.exitFullscreen)
			document.exitFullscreen();
		else if(document.webkitExitFullscreen)
			document.webkitExitFullscreen();
		else if(document.mozCancelFullScreen)
			document.mozCancelFullScreen();
		else if(document.msExitFullscreen)
			document.msExitFullscreen();
		
		bFullscreen = false;					// Set the fullscreen variable
	}
}

// Init function:
function init()
{
	// Code:
	// Get WebGL 2.0 context:
	gl = canvas.getContext("webgl2");
	if(!gl)			// Failure to get context
	{
		console.log("Failure in obtaining WebGL 2.0 rendering context");
		return;
	}
	
	gl.viewportWidth = canvas.width;
	gl.viewportHeight = canvas.height;
	
	// Vertex shader:
	vertexShaderObject = gl.createShader(gl.VERTEX_SHADER);
	
	// Vertex Shader source code:
	var vertexShaderSourceCode = 
	"#version 300 es"+
	"\n"+
	"in vec4 vPosition;"+
	"in vec4 vColor;"+
	"uniform mat4 u_mvp_matrix;"+
	"out vec4 out_color;"+
	"void main(void)"+
	"{"+
	"gl_Position = u_mvp_matrix * vPosition;"+
	"out_color = vColor;"+
	"}";
	
	// Assign the source and compile:
	gl.shaderSource(vertexShaderObject, vertexShaderSourceCode);
	gl.compileShader(vertexShaderObject);
	
	// Error checking:
	if(gl.getShaderParameter(vertexShaderObject, gl.COMPILE_STATUS) == false)
	{
		var error = gl.getShaderInfoLog(vertexShaderObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}
	
	// Fragment shader:
	fragmentShaderObject = gl.createShader(gl.FRAGMENT_SHADER);
	
	// Fragment Shader Source Code:
	var fragmentShaderSourceCode =
	"#version 300 es"+
	"\n"+
	"precision highp float;"+
	"in vec4 out_color;"+
	"out vec4 FragColor;"+
	"void main(void)"+
	"{"+
	"FragColor = out_color;"+
	"}";
	
	// Assign the source and compile:
	gl.shaderSource(fragmentShaderObject, fragmentShaderSourceCode);
	gl.compileShader(fragmentShaderObject);
	
	// Error checking:
	if(gl.getShaderParameter(fragmentShaderObject, gl.COMPILE_STATUS) == false)
	{
		var error = gl.getShaderInfoLog(fragmentShaderObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}
	
	// Shader Program:
	// Create a shader program object and attach the two shaders:
	shaderProgramObject = gl.createProgram();
	gl.attachShader(shaderProgramObject, vertexShaderObject);
	gl.attachShader(shaderProgramObject, fragmentShaderObject);
	
	// pre-link binding of shader program object with vertex shader attributes:
	gl.bindAttribLocation(shaderProgramObject, WebGLMacros.YSG_ATTRIBUTE_POSITION, "vPosition");
	gl.bindAttribLocation(shaderProgramObject, WebGLMacros.YSG_ATTRIBUTE_COLOR, "vColor");

	// Linking:
	gl.linkProgram(shaderProgramObject);
	
	// Error checking:
	if(!gl.getProgramParameter(shaderProgramObject, gl.LINK_STATUS))
	{
		var error = gl.getProgramInfoLog(shaderProgramObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}
	
	// Get MVP uniform locations:
	mvpUniform = gl.getUniformLocation(shaderProgramObject, "u_mvp_matrix");
	
	// Vertex, Color, Normals, Shader attribs, vao, vbo initialization:
	var pyramidVertices = new Float32Array([					
											// Perspective triangle (Front face)
											0.0, 1.0, 0.0,		// Apex
											-1.0, -1.0, 1.0,	// Left bottom
											1.0, -1.0, 1.0,		// Right bottom
											// Perspective triangle (Right face)
											0.0, 1.0, 0.0,		// Apex
											1.0, -1.0, 1.0,		// Left bottom
											1.0, -1.0, -1.0,	// Right bottom
											// Perspective triangle (Back face)
											0.0, 1.0, 0.0,		// Apex
											1.0, -1.0, -1.0,	// Left bottom
											-1.0, -1.0, -1.0,	// Right bottom
											// Perspective triangle (Left face)
											0.0, 1.0, 0.0,		// Apex
											-1.0, -1.0, -1.0,	// Left bottom
											-1.0, -1.0, 1.0		// Right bottom
											]);
											
	var cubeVertices = new Float32Array([						
											// Perspective square (Top face)
											1.0, 1.0, -1.0,		// Right top
											-1.0, 1.0, -1.0, 	// Left top
											-1.0, 1.0, 1.0,		// Left bottom
											1.0, 1.0, 1.0,		// Right bottom
											// Perspective square (Bottom face)
											1.0, -1.0, -1.0,	// Right top
											-1.0, -1.0, -1.0, 	// Left top
											-1.0, -1.0, 1.0,	// Left bottom
											1.0, -1.0, 1.0,		// Right bottom
											// Perspective square (Front face)
											1.0, 1.0, 1.0,		// Right top
											-1.0, 1.0, 1.0,		// Left top
											-1.0, -1.0, 1.0, 	// Left bottom
											1.0, -1.0, 1.0,		// Right bottom
											// Perspective square (Back face)
											1.0, 1.0, -1.0,		// Right top											
											-1.0, 1.0, -1.0,	// Left top
											-1.0, -1.0, -1.0, 	// Left bottom
											1.0, -1.0, -1.0,	// Right bottom
											// Perspective square (Right face)
											1.0, 1.0, -1.0,		// Right top											
											1.0, 1.0, 1.0,		// Left top
											1.0, -1.0, 1.0, 	// Left bottom
											1.0, -1.0, -1.0,	// Right bottom
											// Perspective square (Left face)
											-1.0, 1.0, 1.0,		// Right top																						
											-1.0, 1.0, -1.0,	// Left top
											-1.0, -1.0, -1.0, 	// Left bottom
											-1.0, -1.0, 1.0		// Right bottom
											]);
											
	var pyramidColors = new Float32Array([						// Multi Colored Pyramid				
											1.0, 0.0, 0.0,		// Red apex
											0.0, 1.0, 0.0,		// Green left bottom
											0.0, 0.0, 1.0,		// Blue right bottom
											1.0, 0.0, 0.0,		// Red apex
											0.0, 0.0, 1.0,		// Blue right bottom
											0.0, 1.0, 0.0,		// Green left bottom
											1.0, 0.0, 0.0,		// Red apex
											0.0, 1.0, 0.0,		// Green left bottom
											0.0, 0.0, 1.0,		// Blue right bottom
											1.0, 0.0, 0.0,		// Red apex
											0.0, 0.0, 1.0,		// Blue right bottom
											0.0, 1.0, 0.0		// Green left bottom
											]);
		

	var cubeColors = new Float32Array([						// Multi Colored Cube
										1.0, 0.0, 0.0,		// Red 		- Top face
										1.0, 0.0, 0.0,		// Red
										1.0, 0.0, 0.0,		// Red
										1.0, 0.0, 0.0,		// Red
										0.0, 1.0, 0.0,		// Green 	- Bottom face
										0.0, 1.0, 0.0,		// Green
										0.0, 1.0, 0.0,		// Green											
										0.0, 1.0, 0.0,		// Green
										0.0, 0.0, 1.0,		// Blue		- Front face
										0.0, 0.0, 1.0,		// Blue
										0.0, 0.0, 1.0,		// Blue
										0.0, 0.0, 1.0,		// Blue
										0.0, 1.0, 1.0,		// Cyan		- Back face
										0.0, 1.0, 1.0,		// Cyan
										0.0, 1.0, 1.0,		// Cyan
										0.0, 1.0, 1.0,		// Cyan
										1.0, 0.0, 1.0,		// Magenta 	- Right face
										1.0, 0.0, 1.0,		// Magenta
										1.0, 0.0, 1.0,		// Magenta
										1.0, 0.0, 1.0,		// Magenta
										1.0, 1.0, 0.0,		// Yellow	- Left face
										1.0, 1.0, 0.0,		// Yellow
										1.0, 1.0, 0.0,		// Yellow											
										1.0, 1.0, 0.0		// Yellow
										]);

	
	// A. BLOCK FOR PYRAMID:
	// Bind vao:
	vao_Pyramid = gl.createVertexArray();
	gl.bindVertexArray(vao_Pyramid);
		
	// 1. Buffer block for vertices:
	// Bind vbo:
	vbo_Pyramid_Position = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vbo_Pyramid_Position);
	gl.bufferData(gl.ARRAY_BUFFER, pyramidVertices, gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.YSG_ATTRIBUTE_POSITION,
							3,		// For x, y, z co-ordinates
							gl.FLOAT,
							false, 0, 0);
	gl.enableVertexAttribArray(WebGLMacros.YSG_ATTRIBUTE_POSITION);
	
	// Release the buffer:
	gl.bindBuffer(gl.ARRAY_BUFFER, null);
	
	// 2. Buffer block for colors:
	// Bind vbo:
	vbo_Pyramid_Color = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vbo_Pyramid_Color);
	gl.bufferData(gl.ARRAY_BUFFER, pyramidColors, gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.YSG_ATTRIBUTE_COLOR,
							3,		// For x, y, z co-ordinates
							gl.FLOAT,
							false, 0, 0);
	gl.enableVertexAttribArray(WebGLMacros.YSG_ATTRIBUTE_COLOR);
	
	// Release the buffer:
	gl.bindBuffer(gl.ARRAY_BUFFER, null);
	
	// Release the vao:
	gl.bindVertexArray(null);
	
	// B. BLOCK FOR CUBE:
	// Bind vao:
	vao_Cube = gl.createVertexArray();
	gl.bindVertexArray(vao_Cube);
	
	// 1. Buffer block for vertices:
	// Bind vbo:
	vbo_Cube_Position = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vbo_Cube_Position);
	gl.bufferData(gl.ARRAY_BUFFER, cubeVertices, gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.YSG_ATTRIBUTE_POSITION,
							3,
							gl.FLOAT,
							false, 0, 0);
	gl.enableVertexAttribArray(WebGLMacros.YSG_ATTRIBUTE_POSITION);
	
	// Release the buffer:
	gl.bindBuffer(gl.ARRAY_BUFFER, null);
	
	// 2. Buffer block for colors:
	// Bind vbo:
	
	vbo_Cube_Color = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vbo_Cube_Color);
	gl.bufferData(gl.ARRAY_BUFFER, cubeColors, gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.YSG_ATTRIBUTE_COLOR,
							3,
							gl.FLOAT,
							false, 0, 0);
	gl.enableVertexAttribArray(WebGLMacros.YSG_ATTRIBUTE_COLOR);
	
	// Release the buffer:
	gl.bindBuffer(gl.ARRAY_BUFFER, null);
	
	// Release vao:
	gl.bindVertexArray(null);
	
	// Enable depth testing and sepcify the depth test to perform:
	gl.enable(gl.DEPTH_TEST);
	gl.depthFunc(gl.LEQUAL);
	
	// Set clearColor:
	gl.clearColor(0.0, 0.0, 0.0, 1.0);		// Black background
	
	// Initialize the projection matrix:
	perspectiveProjectionMatrix = mat4.create();
}

// Resize function:
function resize()
{
	// Code:
	if(bFullscreen == true)
	{
		canvas.width = window.innerWidth;
		canvas.height = window.innerHeight;
	}
	else
	{
		canvas.width = canvas_original_width;
		canvas.height = canvas_original_height;
	}
	
	// Set the viewport to match the canvas:
	gl.viewport(0, 0, canvas.width, canvas.height);
	
	// Perspective projection:
    mat4.perspective(perspectiveProjectionMatrix, 45.0, parseFloat(canvas.width) / parseFloat(canvas.height), 0.1, 100.0);

}

// Display/Drawing function:
function draw()
{
	// Code:
	gl.clear(gl.COLOR_BUFFER_BIT);
	gl.useProgram(shaderProgramObject);
	
	// Create and set matrices to identity:
	var modelViewMatrix = mat4.create();
	var modelViewProjectionMatrix = mat4.create();
	
	// A. DRAW PYRAMID:
	// Translate behind:
	// Use directly array syntax (array inilializer) in javascript:
	modelViewMatrix = mat4.translate(modelViewMatrix, modelViewMatrix, [-1.5, 0.0, -6.0]);

	// Rotate pyramid about Y axis:
	mat4.rotateY(modelViewMatrix, modelViewMatrix, degree_to_radian(anglePyramid));
	
	// Multiply modelViewMatrix with perspectiveProjectionMatrix and store in modelViewProjectionMatrix:
	mat4.multiply(modelViewProjectionMatrix, perspectiveProjectionMatrix, modelViewMatrix);
	gl.uniformMatrix4fv(mvpUniform, false, modelViewProjectionMatrix);
	
	// Bind vao:
	gl.bindVertexArray(vao_Pyramid);
	gl.drawArrays(gl.TRIANGLES, 0, 12);
	
	// Unbind vao:
	gl.bindVertexArray(null);
	
	// B. DRAW CUBE:
	
	// Set back to identity:
	modelViewMatrix = mat4.identity(modelViewMatrix);
	modelViewProjectionMatrix = mat4.identity(modelViewProjectionMatrix);

	// Scale the cube by 0.75 from all sides:
	modelViewMatrix = mat4.scale(modelViewMatrix, modelViewMatrix, [0.75, 0.75, 0.75]);
	
	// Translate behind:
	// Use directly array syntax (array inilializer) in javascript:
	modelViewMatrix = mat4.translate(modelViewMatrix, modelViewMatrix, [1.5, 0.0, -6.0]);
	
	// Rotate cube about X, Y and Z axis:
	mat4.rotateX(modelViewMatrix, modelViewMatrix, degree_to_radian(angleCube));
	mat4.rotateY(modelViewMatrix, modelViewMatrix, degree_to_radian(angleCube));
	mat4.rotateZ(modelViewMatrix, modelViewMatrix, degree_to_radian(angleCube));
	
	// Multiply modelViewMatrix with perspectiveProjectionMatrix and store in modelViewProjectionMatrix:
	mat4.multiply(modelViewProjectionMatrix, perspectiveProjectionMatrix, modelViewMatrix);
	gl.uniformMatrix4fv(mvpUniform, false, modelViewProjectionMatrix);
	
	// Bind vao:
	gl.bindVertexArray(vao_Cube);
	gl.drawArrays(gl.TRIANGLE_FAN, 0, 4);
	gl.drawArrays(gl.TRIANGLE_FAN, 4, 4);
	gl.drawArrays(gl.TRIANGLE_FAN, 8, 4);
	gl.drawArrays(gl.TRIANGLE_FAN, 12, 4);
	gl.drawArrays(gl.TRIANGLE_FAN, 16, 4);
	gl.drawArrays(gl.TRIANGLE_FAN, 20, 4);
	
	// Unbind vao:
	gl.bindVertexArray(null);
	
	// Stop using the program object:
	gl.useProgram(null);
	
	// Update for rotation:
	update();
	
	// Animation loop/Game loop:
	// We use a function pointer to call the function and pass the same function name as a parameter so
	// that the function is called in a loop and thus forming the game loop:
	requestAnimationFrame(draw, canvas);
}

function update()
{
	anglePyramid = anglePyramid + 1.0;
	if(anglePyramid >= 360.0)
		anglePyramid = 0.0;

	angleCube = angleCube - 1.0;
	if(angleCube <= -360.0)
		angleCube = 0.0;
}

function uninitialize()
{
	// Code:
	// Destroy vao for pyramid:
	if(vao_Pyramid)
	{
		gl.deleteVertexArray(vao_Pyramid);
		vao_Pyramid = null;
	}
	
	// Destroy vao for cube:
	if(vao_Cube)
	{
		gl.deleteVertexArray(vao_Cube);
		vao_Cube = null;
	}
	
	// Destroy vbo for pyramid position:
	if(vbo_Pyramid_Position)
	{
		gl.deleteBuffer(vbo_Pyramid_Position);
		vbo_Pyramid_Position = null;
	}

	// Destroy vbo for pyramid colors:
	if(vbo_Pyramid_Color)
	{
		gl.deleteBuffer(vbo_Pyramid_Color);
		vbo_Pyramid_Color = null;
	}
	
	// Destroy vbo for cube position:
	if(vbo_cube_Position)
	{
		gl.deleteBuffer(vbo_cube_Position);
		vbo_cube_Position = null;
	}

	// Destroy vbo for cube color:
	if(vbo_Cube_Color)
	{
		gl.deleteBuffer(vbo_Cube_Color);
		vbo_Cube_Color = null;
	}
	
	
	if(shaderProgramObject)
	{
		if(vertexShaderObject)
		{
			gl.detachShader(shaderProgramObject, vertexShaderObject);
			gl.deleteShader(vertexShaderObject);
			vertexShaderObject = null;
		}
		
		if(fragmentShaderObject)
		{
			gl.detachShader(shaderProgramObject, fragmentShaderObject);
			gl.deleteShader(fragmentShaderObject);
			fragmentShaderObject = null;
		}
		
		gl.deleteProgram(shaderProgramObject);
		shaderProgramObject = null;
	}
}

// Converts the angle from degrees to radians as required by rotateX/Y/Z function:
function degree_to_radian(degrees)
{
	return(degrees * Math.PI / 180.0);
}
		
// Event Handling functions:
function keyDown()
{
	// Code:
	switch(event.keyCode)
	{
		case 27:		// Escape
			// uninitialize:
			uninitialize();
			window.close();		// Close our application's tab (may not work in Firefox)
			break;
			
		case 70:		// For 'F' or 'f':
			toggleFullScreen();
			break;
	}
}

function mouseDown()
{
	// Code:
}
