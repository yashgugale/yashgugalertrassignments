#include <windows.h>
#include <stdio.h>		// For file I/O

#include <gl\glew.h>	// For GLSL extensions. It must be included before <gl\GL.h>
#include <gl\GL.h>

#include "vmath.h"		// Header file from Red Book (For maths) - (v-vermilion)
#include "Sphere.h"		// For the sphere object

#pragma comment(lib,"glew32.lib")
#pragma comment(lib,"opengl32.lib")
#pragma comment(lib,"Sphere.lib")

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

using namespace vmath;

// Properties of the vertex
enum
{
	YSG_ATTRIBUTE_POSITION = 0,
	YSG_ATTRIBUTE_COLOR,
	YSG_ATTRIBUTE_NORMAL,
	YSG_ATTRIBUTE_TEXTURE0
};

//Prototype Of WndProc() declared Globally
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

//Global variable declarations
FILE *gpFile = NULL;		// Global pointer, so that log file can be opened anywhere

HWND ghwnd = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;

DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };

bool gbActiveWindow = false;
bool gbEscapeKeyIsPressed = false;
bool gbFullscreen = false;

GLuint gVertexShaderObject;
GLuint gFragmentShaderObject;
GLuint gShaderProgramObject;

// Sphere variables:
float sphere_vertices[1146];
float sphere_normals[1146];
float sphere_textures[764];
unsigned short sphere_elements[2280];

// Light rotation variables:
float angle_RedLight = 0.0f;
float angle_GreenLight = 0.0f;
float angle_BlueLight = 0.0f;

GLuint gNumVertices;
GLuint gNumElements;

// Sphere Vao and Vbo:
GLuint gVaoSphere;
GLuint gVboSpherePosition;
GLuint gVboSphereNormal;
GLuint gVboSphereElement;

// Uniforms:
GLuint gModelMatrixUniform, gViewMatrixUniform, gProjectionMatrixUniform;

// Light uniforms:
// 1. RED LIGHT:
GLuint gLaUniformRed;				// Uniform for ambient component of light
GLuint gLdUniformRed;				// Uniform for diffuse component of light
GLuint gLsUniformRed;				// Uniform for specular component of light
GLuint gRedLightPositionUniform;	// Uniform for light position

// 2. GREEN LIGHT:
GLuint gLaUniformGreen;				// Uniform for ambient component of light
GLuint gLdUniformGreen;				// Uniform for diffuse component of light
GLuint gLsUniformGreen;				// Uniform for specular component of light
GLuint gGreenLightPositionUniform;	// Uniform for light position

// 3. BLUE LIGHT:
GLuint gLaUniformBlue;				// Uniform for ambient component of light
GLuint gLdUniformBlue;				// Uniform for diffuse component of light
GLuint gLsUniformBlue;				// Uniform for specular component of light
GLuint gBlueLightPositionUniform;	// Uniform for light position

// Matrial uniforms:
GLuint gKaUniform;					// Uniform for ambient component of material
GLuint gKdUniform;					// Uniform for diffuse component of material
GLuint gKsUniform;					// Uniform for specular component of material
GLuint gMaterialShininessUniform;	// Uniform for shininess component of material

GLuint gLKeyPressedUniform;			// To enable lighting
GLuint gFKeyPressedUniform;			// To toggle to Fragment shader
GLuint gVKeyPressedUniform;			// To toggle to Vertex shader

mat4 gPerspectiveProjectionMatrix;

// Animate and Light variables:
bool gbLight = false;
bool gbVertexShader = true;
bool gbFragmentShader = false;

// Lighting arrays:
// RED LIGHT:
GLfloat light_Red_ambient[] = { 0.0f, 0.0f, 0.0f, 1.0f };
GLfloat light_Red_diffuse[] = { 1.0f, 0.0f, 0.0f, 1.0f };		//Red light
GLfloat light_Red_specular[] = { 1.0f, 0.0f, 0.0f, 1.0f };		//Red light
GLfloat light_Red_position[] = { 0.0f, 0.0f, 0.0f, 0.0f };		//Red light rotates about X-axis

// GREEN LIGHT:
GLfloat light_Green_ambient[] = { 0.0f, 0.0f, 0.0f, 1.0f };
GLfloat light_Green_diffuse[] = { 0.0f, 1.0f, 0.0f, 1.0f };		//Green light
GLfloat light_Green_specular[] = { 0.0f, 1.0f, 0.0f, 1.0f };	//Green light
GLfloat light_Green_position[] = { 0.0f, 0.0f, 0.0f, 0.0f };	//Green light rotates about Y-axis

// BLUE LIGHT:
GLfloat light_Blue_ambient[] = { 0.0f, 0.0f, 0.0f, 1.0f };
GLfloat light_Blue_diffuse[] = { 0.0f, 0.0f, 1.0f, 1.0f };		//Blue light
GLfloat light_Blue_specular[] = { 0.0f, 0.0f, 1.0f, 1.0f };		//Blue light
GLfloat light_Blue_position[] = { 0.0f, 0.0f, 0.0f, 0.0f };		//Blue light rotates about Z-axis

// MATERIAL:
GLfloat material_ambient[] = { 0.0f, 0.0f, 0.0f, 1.0f };
GLfloat material_diffuse[] = { 1.0f, 1.0f, 1.0f, 1.0f };
GLfloat material_specular[] = { 1.0f, 1.0f, 1.0f, 1.0f };
GLfloat material_shininess = 50.0f;

//main()
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	//function prototype
	void initialize(void);
	void uninitialize(void);
	void display(void);
	void update(void);

	//variable declaration
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szClassName[] = TEXT("OpenGLPP");
	bool bDone = false;

	//code
	// Create log file for debugging
	if (fopen_s(&gpFile, "Log.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("Log File Can Not Be Created\nExitting ..."), TEXT("Error"), MB_OK | MB_TOPMOST | MB_ICONSTOP);
		exit(0);
	}
	else
	{
		fprintf(gpFile, "Log File Is Successfully Opened.\n");
	}

	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.hInstance = hInstance;
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.lpfnWndProc = WndProc;
	wndclass.lpszClassName = szClassName;
	wndclass.lpszMenuName = NULL;

	RegisterClassEx(&wndclass);

	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szClassName,
		TEXT("OpenGL Programmable Pipeline : Lights on Sphere (Per Vertex lighting)"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		100,
		100,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;

	ShowWindow(hwnd, iCmdShow);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	// Initialize
	initialize();

	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
				bDone = true;
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			// Update for rotation:
			update();
			// Drawing / rendering function
			display();
			if (gbActiveWindow == true)
			{
				if (gbEscapeKeyIsPressed == true)
					bDone = true;
			}
		}
	}

	uninitialize();

	return((int)msg.wParam);
}

//WndProc()
LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	//function prototype
	void resize(int, int);
	void ToggleFullscreen(void);
	void uninitialize(void);

	// Variable declarations:
	static WORD xMouse = NULL;
	static WORD yMouse = NULL;

	//code
	switch (iMsg)
	{
	case WM_ACTIVATE:
		if (HIWORD(wParam) == 0)		// If the window is active
			gbActiveWindow = true;
		else							// If non-zero, the window is inactive
			gbActiveWindow = false;
		break;
	case WM_ERASEBKGND:
		return(0);
	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam));
		break;
	case WM_KEYDOWN:
		switch (wParam)
		{
		case 0x51:		// 'Q' key is for quit instead of ESC
			gbEscapeKeyIsPressed = true;
			break;
		case VK_ESCAPE:		// 'ESC' key is pressed for fullscreen
			if (gbFullscreen == false)
			{
				ToggleFullscreen();
				gbFullscreen = true;
			}
			else
			{
				ToggleFullscreen();
				gbFullscreen = false;
			}
			break;
		case 0x46:		// 'F' key is pressed to toggle to fragment shader
			if (gbFragmentShader == false)
			{
				gbFragmentShader = true;
				gbVertexShader = false;
			}
			break;
		case 0x56:		// 'V' key is pressed to toggle to vertex shader
			if (gbVertexShader == false)
			{
				gbVertexShader = true;
				gbFragmentShader = false;
			}
			break;
		case 0x4C:		// for 'L' or 'l'
			if (gbLight == false)
			{
				gbLight = true;
			}
			else
			{
				gbLight = false;
			}
			break;
		default:
			break;
		}
		break;
	case WM_LBUTTONDOWN:
		break;
	case WM_CLOSE:
		uninitialize();
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggleFullscreen(void)
{
	//variable declarations
	MONITORINFO mi;

	//code
	if (gbFullscreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);
	}

	else
	{
		//code
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

		ShowCursor(TRUE);
	}
}

void initialize(void)
{
	//function prototypes
	void uninitialize(void);
	void resize(int, int);

	//variable declarations
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	//code
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 32;

	ghdc = GetDC(ghwnd);

	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
	if (iPixelFormatIndex == 0)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == false)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (wglMakeCurrent(ghdc, ghrc) == false)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	// GLEW initializtion code for GLSL.
	// It must be here, ie, after creating OpenGL context but before using and OpenGL functions
	GLenum glew_error = glewInit();
	if (glew_error != GLEW_OK)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	/*
	GLint number_of_extensions;
	glGetIntegerv(GL_NUM_EXTENSIONS, &number_of_extensions);		// Returns the number of available extensions
	fprintf(gpFile, "\nPrinting the available extensions : \n");
	for (int i = 0; i < number_of_extensions; i++)
	{
	fprintf(gpFile, "%d. %s\n", i+1, (const char *)glGetStringi(GL_EXTENSIONS, i));
	}
	fprintf(gpFile, "\n");
	*/

	// *** VERTEX SHADER ***
	// 1. Create the shader (object is created and assigned)
	gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

	// 2. Provide the source code to shader
	// '\' indicates a single string and not 3 separate strings. Source code as array of characters.
	// Instead of keeping inside a file (eg vertexshader.vsh), we keep it as a string inside our source file

	// GLSL version number 1.3 (1.3 * 100 = 130). default = 1.1
	// vPosition and u_mvp_matrix are user defined names for matrices for transformation

	// PER VERTEX LIGHTING:
	const GLchar *vertexShaderSourceCode =
		"#version 450 core" \
		"\n" \
		"in vec4 vPosition;" \
		"in vec3 vNormal;" \
		"uniform mat4 u_model_matrix;" \
		"uniform mat4 u_view_matrix;" \
		"uniform mat4 u_projection_matrix;" \
		"uniform int u_lighting_enabled;" \
		"uniform int u_per_vertex_lighting;"	\
		"uniform int u_per_fragment_lighting;"	\

		"uniform vec3 u_La_Red;" \
		"uniform vec3 u_Ld_Red;" \
		"uniform vec3 u_Ls_Red;" \
		"uniform vec4 u_light_position_Red;" \

		"uniform vec3 u_La_Green;" \
		"uniform vec3 u_Ld_Green;" \
		"uniform vec3 u_Ls_Green;" \
		"uniform vec4 u_light_position_Green;" \

		"uniform vec3 u_La_Blue;" \
		"uniform vec3 u_Ld_Blue;" \
		"uniform vec3 u_Ls_Blue;" \
		"uniform vec4 u_light_position_Blue;" \

		"uniform vec3 u_Ka;" \
		"uniform vec3 u_Kd;" \
		"uniform vec3 u_Ks;" \
		"uniform float u_material_shininess;" \

		"vec3 phong_ads_color_Red;"	\
		"vec3 phong_ads_color_Green;"	\
		"vec3 phong_ads_color_Blue;"	\
		"out vec3 vPhong_ads_color;" \

		"out vec4 eye_coordinates;" \
		"out vec3 transformed_normals;" \

		"out vec3 light_direction_Red;" \
		"out vec3 light_direction_Green;" \
		"out vec3 light_direction_Blue;" \
		"out vec3 viewer_vector;" \

		"void main(void)" \
		"{" \
		"if(u_lighting_enabled==1)" \
		"{" \

		"if(u_per_vertex_lighting==1)"	\
		"{"	\
		"vec4 eye_coordinates=u_view_matrix * u_model_matrix * vPosition;" \
		"vec3 transformed_normals=normalize(mat3(u_view_matrix * u_model_matrix) * vNormal);" \

		"vec3 light_direction_Red = normalize(vec3(u_light_position_Red) - eye_coordinates.xyz);" \
		"vec3 light_direction_Green = normalize(vec3(u_light_position_Green) - eye_coordinates.xyz);" \
		"vec3 light_direction_Blue = normalize(vec3(u_light_position_Blue) - eye_coordinates.xyz);" \

		"float tn_dot_ld_Red = max(dot(transformed_normals, light_direction_Red),0.0);" \
		"float tn_dot_ld_Green = max(dot(transformed_normals, light_direction_Green),0.0);" \
		"float tn_dot_ld_Blue = max(dot(transformed_normals, light_direction_Blue),0.0);" \

		"vec3 ambient_Red = u_La_Red * u_Ka;" \
		"vec3 ambient_Green = u_La_Green * u_Ka;" \
		"vec3 ambient_Blue = u_La_Blue * u_Ka;" \

		"vec3 diffuse_Red = u_Ld_Red * u_Kd * tn_dot_ld_Red;" \
		"vec3 diffuse_Green = u_Ld_Green * u_Kd * tn_dot_ld_Green;" \
		"vec3 diffuse_Blue = u_Ld_Blue * u_Kd * tn_dot_ld_Blue;" \

		"vec3 reflection_vector_Red = reflect(-light_direction_Red, transformed_normals);" \
		"vec3 reflection_vector_Green = reflect(-light_direction_Green, transformed_normals);" \
		"vec3 reflection_vector_Blue = reflect(-light_direction_Blue, transformed_normals);" \

		"vec3 viewer_vector = normalize(-eye_coordinates.xyz);" \

		"vec3 specular_Red = u_Ls_Red * u_Ks * pow(max(dot(reflection_vector_Red, viewer_vector), 0.0), u_material_shininess);" \
		"vec3 specular_Green = u_Ls_Green * u_Ks * pow(max(dot(reflection_vector_Green, viewer_vector), 0.0), u_material_shininess);" \
		"vec3 specular_Blue = u_Ls_Blue * u_Ks * pow(max(dot(reflection_vector_Blue, viewer_vector), 0.0), u_material_shininess);" \

		"phong_ads_color_Red=ambient_Red + diffuse_Red + specular_Red;" \
		"phong_ads_color_Green=ambient_Green + diffuse_Green + specular_Green;" \
		"phong_ads_color_Blue=ambient_Blue + diffuse_Blue + specular_Blue;" \
		"vPhong_ads_color = phong_ads_color_Red + phong_ads_color_Green + phong_ads_color_Blue;"
		"}" \
		"else"	\
		"{"	\
		"eye_coordinates=u_view_matrix * u_model_matrix * vPosition;" \
		"transformed_normals=mat3(u_view_matrix * u_model_matrix) * vNormal;" \

		"light_direction_Red = vec3(u_light_position_Red) - eye_coordinates.xyz;" \
		"light_direction_Green = vec3(u_light_position_Green) - eye_coordinates.xyz;" \
		"light_direction_Blue = vec3(u_light_position_Blue) - eye_coordinates.xyz;" \
		"viewer_vector = -eye_coordinates.xyz;" \
		"}"	\
		"}"	\
		"else" \
		"{" \
		"vPhong_ads_color = vec3(1.0, 1.0, 1.0);" \
		"}" \
		"gl_Position=u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;" \
		"}";

	// 3. Pass the object as the shader source. glVertexShaderObject is our specialist
	// 1 - 1program, so only 1 string. NULL - Pass length of string if not NUL terminated
	// (const GLchar **) - Pass the address of the program by casting (as array is used)
	glShaderSource(gVertexShaderObject, 1, (const GLchar **)&vertexShaderSourceCode, NULL);

	// 4. Compile the shader - Dynamic compilation by the driver
	glCompileShader(gVertexShaderObject);

	// ERROR CHECKING:
	GLint iInfoLogLength = 0;			// Length of the log created on failure to compile
	GLint iShaderCompiledStatus = 0;	// Stores shader compilation status
	char *szInfoLog = NULL;				// String to store the log

	glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);
	if (iShaderCompiledStatus == GL_FALSE)	// Failure to compile
	{
		glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;		// Stores number of characters written
				glGetShaderInfoLog(gVertexShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Vertex shader compilation log : %s\n", szInfoLog);
				free(szInfoLog);
				szInfoLog = NULL;
				uninitialize();
				exit(0);
			}
		}
	}

	// *** FRAGMENT SHADER ***
	// Create the shader
	gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

	// Pass the source code to shader
	// FragColor is our name for the Fragment
	const GLchar *fragmentShaderSourceCode =
		"#version 450 core"	\
		"\n" \
		"uniform int u_per_vertex_lighting;"	\
		"uniform int u_per_fragment_lighting;"	\

		"in vec3 vPhong_ads_color;" \
		"out vec4 FragColor;" \

		"in vec3 transformed_normals;"	\
		"in vec3 light_direction_Red;"	\
		"in vec3 light_direction_Green;"	\
		"in vec3 light_direction_Blue;"	\

		"in vec3 viewer_vector;"	\

		"uniform vec3 u_La_Red;" \
		"uniform vec3 u_Ld_Red;" \
		"uniform vec3 u_Ls_Red;" \

		"uniform vec3 u_La_Green;" \
		"uniform vec3 u_Ld_Green;" \
		"uniform vec3 u_Ls_Green;" \

		"uniform vec3 u_La_Blue;" \
		"uniform vec3 u_Ld_Blue;" \
		"uniform vec3 u_Ls_Blue;" \

		"uniform vec3 u_Ka;" \
		"uniform vec3 u_Kd;" \
		"uniform vec3 u_Ks;" \
		"uniform float u_material_shininess;" \

		"uniform int u_lighting_enabled;" \

		"void main(void)" \
		"{" \
		"vec3 phong_ads_color;"
		"if(u_lighting_enabled==1)" \
		"{" \
		"if(u_per_fragment_lighting==1)"	\
		"{"	\
		"vec3 normalized_transformed_normals=normalize(transformed_normals);" \
		"vec3 normalized_light_direction_Red=normalize(light_direction_Red);" \
		"vec3 normalized_light_direction_Green=normalize(light_direction_Green);" \
		"vec3 normalized_light_direction_Blue=normalize(light_direction_Blue);" \
		"vec3 normalized_viewer_vector=normalize(viewer_vector);" \

		"vec3 ambient_Red = u_La_Red * u_Ka;" \
		"vec3 ambient_Green = u_La_Green * u_Ka;" \
		"vec3 ambient_Blue = u_La_Blue * u_Ka;" \

		"float tn_dot_ld_Red = max(dot(normalized_transformed_normals, normalized_light_direction_Red),0.0);" \
		"float tn_dot_ld_Green = max(dot(normalized_transformed_normals, normalized_light_direction_Green),0.0);" \
		"float tn_dot_ld_Blue = max(dot(normalized_transformed_normals, normalized_light_direction_Blue),0.0);" \

		"vec3 diffuse_Red = u_Ld_Red * u_Kd * tn_dot_ld_Red;" \
		"vec3 diffuse_Green = u_Ld_Green * u_Kd * tn_dot_ld_Green;" \
		"vec3 diffuse_Blue = u_Ld_Blue * u_Kd * tn_dot_ld_Blue;" \

		"vec3 reflection_vector_Red = reflect(-normalized_light_direction_Red, normalized_transformed_normals);" \
		"vec3 reflection_vector_Green = reflect(-normalized_light_direction_Green, normalized_transformed_normals);" \
		"vec3 reflection_vector_Blue = reflect(-normalized_light_direction_Blue, normalized_transformed_normals);" \

		"vec3 specular_Red = u_Ls_Red * u_Ks * pow(max(dot(reflection_vector_Red, normalized_viewer_vector), 0.0), u_material_shininess);" \
		"vec3 specular_Green = u_Ls_Green * u_Ks * pow(max(dot(reflection_vector_Green, normalized_viewer_vector), 0.0), u_material_shininess);" \
		"vec3 specular_Blue = u_Ls_Blue * u_Ks * pow(max(dot(reflection_vector_Blue, normalized_viewer_vector), 0.0), u_material_shininess);" \

		"vec3 phong_ads_color_Red = ambient_Red + diffuse_Red + specular_Red;"	\
		"vec3 phong_ads_color_Green = ambient_Green + diffuse_Green + specular_Green;"	\
		"vec3 phong_ads_color_Blue = ambient_Blue + diffuse_Blue + specular_Blue;"	\
		"phong_ads_color = phong_ads_color_Red + phong_ads_color_Green + phong_ads_color_Blue;"	\
		"}"	\
		"else"	\
		"{"	\
		"phong_ads_color = vPhong_ads_color;"
		"}"	\
		"}"	\
		"else"	\
		"{"	\
		"phong_ads_color = vec3(1.0, 1.0, 1.0);"	\
		"}"	\
		"FragColor = vec4(phong_ads_color, 1.0);" \
		"}";

	glShaderSource(gFragmentShaderObject, 1, (const GLchar **)&fragmentShaderSourceCode, NULL);

	// compiler the shader
	glCompileShader(gFragmentShaderObject);

	// Error checking for compilation done here
	glGetShaderiv(gFragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);
	if (iShaderCompiledStatus == GL_FALSE)	// Failure to compile
	{
		glGetShaderiv(gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;		// Stores number of characters written
				glGetShaderInfoLog(gFragmentShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Fragment shader compilation log : %s\n", szInfoLog);
				free(szInfoLog);
				szInfoLog = NULL;
				uninitialize();
				exit(0);
			}
		}
	}
	// *** SHADER PROGRAM ***
	// Create
	gShaderProgramObject = glCreateProgram();		// It can link ALL the shaders. Hence, no parameter

													// Attach vertex shader to the shader program
	glAttachShader(gShaderProgramObject, gVertexShaderObject);

	// Attach fragment shader to the shader program
	glAttachShader(gShaderProgramObject, gFragmentShaderObject);

	// Pre link binding of shader program object with vertex shader position attribute
	// This shader object is recognized by out YSG_ATTRIBUTE_POSITION
	glBindAttribLocation(gShaderProgramObject, YSG_ATTRIBUTE_POSITION, "vPosition");

	// Pre link binding of shader program object with fragment shader position attribute
	// This shader object is recognized by out YSG_ATTRIBUTE_NORMAL
	glBindAttribLocation(gShaderProgramObject, YSG_ATTRIBUTE_NORMAL, "vNormal");

	// Link the shader
	glLinkProgram(gShaderProgramObject);

	// Error checking for linking
	GLint iShaderProgramLinkStatus = 0;

	glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iShaderProgramLinkStatus);
	if (iShaderProgramLinkStatus == GL_FALSE)	// Failure to link
	{
		glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;		// Stores number of characters written
				glGetProgramInfoLog(gShaderProgramObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Program shader link log : %s\n", szInfoLog);
				free(szInfoLog);
				szInfoLog = NULL;
				uninitialize();
				exit(0);
			}
		}
	}

	// Get uniform locations:
	// Uniforms for Model, View and Projection matrices:
	gModelMatrixUniform = glGetUniformLocation(gShaderProgramObject, "u_model_matrix");
	gViewMatrixUniform = glGetUniformLocation(gShaderProgramObject, "u_view_matrix");
	gProjectionMatrixUniform = glGetUniformLocation(gShaderProgramObject, "u_projection_matrix");

	// If L key is Pressed or not:
	gLKeyPressedUniform = glGetUniformLocation(gShaderProgramObject, "u_lighting_enabled");

	// If V key is Pressed or not:
	gVKeyPressedUniform = glGetUniformLocation(gShaderProgramObject, "u_per_vertex_lighting");

	// If F key is Pressed or not:
	gFKeyPressedUniform = glGetUniformLocation(gShaderProgramObject, "u_per_fragment_lighting");

	// Uniforms for Red Light:
	// Ambient color intensity of light:
	gLaUniformRed = glGetUniformLocation(gShaderProgramObject, "u_La_Red");
	// Diffuse color intensity of light:
	gLdUniformRed = glGetUniformLocation(gShaderProgramObject, "u_Ld_Red");
	// Specular color intensity of light:
	gLsUniformRed = glGetUniformLocation(gShaderProgramObject, "u_Ls_Red");
	// Position of light:
	gRedLightPositionUniform = glGetUniformLocation(gShaderProgramObject, "u_light_position_Red");

	// Uniforms for Green Light:
	// Ambient color intensity of light:
	gLaUniformGreen = glGetUniformLocation(gShaderProgramObject, "u_La_Green");
	// Diffuse color intensity of light:
	gLdUniformGreen = glGetUniformLocation(gShaderProgramObject, "u_Ld_Green");
	// Specular color intensity of light:
	gLsUniformGreen = glGetUniformLocation(gShaderProgramObject, "u_Ls_Green");
	// Position of light:
	gGreenLightPositionUniform = glGetUniformLocation(gShaderProgramObject, "u_light_position_Green");

	// Uniforms for Blue Light:
	// Ambient color intensity of light:
	gLaUniformBlue = glGetUniformLocation(gShaderProgramObject, "u_La_Blue");
	// Diffuse color intensity of light:
	gLdUniformBlue = glGetUniformLocation(gShaderProgramObject, "u_Ld_Blue");
	// Specular color intensity of light:
	gLsUniformBlue = glGetUniformLocation(gShaderProgramObject, "u_Ls_Blue");
	// Position of light:
	gBlueLightPositionUniform = glGetUniformLocation(gShaderProgramObject, "u_light_position_Blue");

	// Uniforms for material:
	// ambient reflective color intensity of material
	gKaUniform = glGetUniformLocation(gShaderProgramObject, "u_Ka");
	// diffuse reflective color intensity of material
	gKdUniform = glGetUniformLocation(gShaderProgramObject, "u_Kd");
	// specular reflective color intensity of material
	gKsUniform = glGetUniformLocation(gShaderProgramObject, "u_Ks");
	// shininess of material ( value is conventionally between 1 to 200 )
	gMaterialShininessUniform = glGetUniformLocation(gShaderProgramObject, "u_material_shininess");;

	// Vertices, Colors, Shader attributes, vbo, vao initializations:
	getSphereVertexData(sphere_vertices, sphere_normals, sphere_textures, sphere_elements);
	gNumVertices = getNumberOfSphereVertices();
	gNumElements = getNumberOfSphereElements();

	// BLOCK FOR SPHERE:
	glGenVertexArrays(1, &gVaoSphere);
	glBindVertexArray(gVaoSphere);

	// A. BUFFER BLOCK FOR VERTICES:
	glGenBuffers(1, &gVboSpherePosition);					// Buffer to store vertex position
	glBindBuffer(GL_ARRAY_BUFFER, gVboSpherePosition);		// Find that named object in memory
	glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_vertices), sphere_vertices, GL_STATIC_DRAW);	// Takes data from CPU to GPU

	glVertexAttribPointer(YSG_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(YSG_ATTRIBUTE_POSITION);

	// Release the buffer for vertices:
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// B. BUFFER BLOCK FOR NORMALS:
	glGenBuffers(1, &gVboSphereNormal);					// Buffer to store vertex normals
	glBindBuffer(GL_ARRAY_BUFFER, gVboSphereNormal);		// Find that named object in memory
	glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_normals), sphere_normals, GL_STATIC_DRAW);	// Takes data from CPU to GPU

	glVertexAttribPointer(YSG_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(YSG_ATTRIBUTE_NORMAL);

	// C. BUFFER BLOCK FOR ELEMENTS:
	glGenBuffers(1, &gVboSphereElement);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVboSphereElement);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(sphere_elements), sphere_elements, GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	// Release the buffer for colors:
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindVertexArray(0);				// Unbind

	glShadeModel(GL_SMOOTH);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
	//glEnable(GL_CULL_FACE);			// Turn off culling for rotation

	// Set the background color to black:
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

	// Set PerspectiveMatrix to identity matrix
	gPerspectiveProjectionMatrix = mat4::identity();

	// Warm up call. (Not required for Windows as resize is called before WM_PAINT
	resize(WIN_WIDTH, WIN_HEIGHT);
}

void display(void)
{
	//code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// Start to use the OpenGL program object
	glUseProgram(gShaderProgramObject);		// Compiled and linked program goes to the driver

	if (gbLight == true)
	{
		// Set the 'u_lighting_enabled' uniform:
		glUniform1i(gLKeyPressedUniform, 1);

		if (gbVertexShader == true)
		{
			glUniform1i(gVKeyPressedUniform, 1);
			glUniform1i(gFKeyPressedUniform, 0);
		}
		if (gbFragmentShader == true)
		{
			glUniform1i(gFKeyPressedUniform, 1);
			glUniform1i(gVKeyPressedUniform, 0);
		}

		// Set the light's properties:
		// 1. RED:
		glUniform3fv(gLaUniformRed, 1, light_Red_ambient);
		glUniform3fv(gLdUniformRed, 1, light_Red_diffuse);
		glUniform3fv(gLsUniformRed, 1, light_Red_specular);
		glUniform4fv(gRedLightPositionUniform, 1, light_Red_position);

		// 2. GREEN:
		glUniform3fv(gLaUniformGreen, 1, light_Green_ambient);
		glUniform3fv(gLdUniformGreen, 1, light_Green_diffuse);
		glUniform3fv(gLsUniformGreen, 1, light_Green_specular);
		glUniform4fv(gGreenLightPositionUniform, 1, light_Green_position);
		
		// 3. BLUE:
		glUniform3fv(gLaUniformBlue, 1, light_Blue_ambient);
		glUniform3fv(gLdUniformBlue, 1, light_Blue_diffuse);
		glUniform3fv(gLsUniformBlue, 1, light_Blue_specular);
		glUniform4fv(gBlueLightPositionUniform, 1, light_Blue_position);

		// Set the material's properties:
		glUniform3fv(gKaUniform, 1, material_ambient);
		glUniform3fv(gKdUniform, 1, material_diffuse);
		glUniform3fv(gKsUniform, 1, material_specular);
		glUniform1f(gMaterialShininessUniform, material_shininess);
	}
	else
	{
		// Set the 'u_lighting_enabled' uniform:
		glUniform1i(gLKeyPressedUniform, 0);
	}

	// OpenGL Drawing
	light_Red_position[1] = cos(angle_RedLight) * 100.0f;
	light_Red_position[2] = sin(angle_RedLight) * 100.0f;
	
	light_Green_position[0] = sin(angle_GreenLight) * 100;
	light_Green_position[2] = cos(angle_GreenLight) * 100;

	light_Blue_position[0] = cos(angle_BlueLight) * 100;
	light_Blue_position[1] = sin(angle_BlueLight) * 100;

	// SPHERE:
	// Set the modelview, rotation, scale and modelviewprojection matrices to identity 
	mat4 modelMatrix = mat4::identity();
	mat4 viewMatrix = mat4::identity();

	// Translate the modelViewMatrix along the z axis
	modelMatrix = translate(0.0f, 0.0f, -2.0f);

	glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(gViewMatrixUniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(gProjectionMatrixUniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);

	// BIND vao
	glBindVertexArray(gVaoSphere);

	// Draw either by glDrawTraingles() or glDrawArrays() or glDrawElements()
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVboSphereElement);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	// Unbind vao
	glBindVertexArray(0);

	// Stop using the OpenGL program object
	glUseProgram(0);

	SwapBuffers(ghdc);
}

void update(void)
{
	//code
	//Red light movement
	angle_RedLight = angle_RedLight - 0.001f;
	if (angle_RedLight <= -360.0f)
		angle_RedLight = 0.0f;

	//Green light movement
	angle_GreenLight = angle_GreenLight - 0.001f;
	if (angle_GreenLight <= -360.0f)
		angle_GreenLight = 0.0f;

	//Blue light movement
	angle_BlueLight = angle_BlueLight - 0.001f;
	if (angle_BlueLight <= -360.0f)
		angle_BlueLight = 0.0f;
}

void resize(int width, int height)
{
	//code
	if (height == 0)
		height = 1;
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);

	// glPerspective(FOV, aspect ratio, near, far)
	gPerspectiveProjectionMatrix = perspective(45.0f, ((GLfloat)width / (GLfloat)height), 0.1f, 100.0f);
}

void uninitialize(void)
{
	//code
	if (gbFullscreen == true)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

		ShowCursor(TRUE);

	}

	// Destroy Sphere vao 
	if (gVaoSphere)
	{
		glDeleteVertexArrays(1, &gVaoSphere);
		gVaoSphere = 0;
	}

	// Destroy vbo for Sphere Position:
	if (gVboSpherePosition)
	{
		glDeleteBuffers(1, &gVboSpherePosition);
		gVboSpherePosition = 0;
	}

	// Destroy vbo for Sphere Normal:
	if (gVboSphereNormal)
	{
		glDeleteBuffers(1, &gVboSphereNormal);
		gVboSphereNormal = 0;
	}

	// Destroy vbo for Sphere Element:
	if (gVboSphereElement)
	{
		glDeleteBuffers(1, &gVboSphereElement);
		gVboSphereElement = 0;
	}

	// Detach the shaders first before deleting
	// Detach vertex shader from shader program object
	glDetachShader(gShaderProgramObject, gVertexShaderObject);
	// Detach fragment shader from shader program object
	glDetachShader(gShaderProgramObject, gFragmentShaderObject);

	// Delete vertex shader object
	glDeleteShader(gVertexShaderObject);
	gVertexShaderObject = 0;
	// Delete fragment shader object
	glDeleteShader(gFragmentShaderObject);
	gFragmentShaderObject = 0;

	// Delete shader program object. It has no objects attached
	glDeleteProgram(gShaderProgramObject);
	gShaderProgramObject = 0;

	// Unlink shader program
	glUseProgram(0);		// Stray call

	wglMakeCurrent(NULL, NULL);

	wglDeleteContext(ghrc);
	ghrc = NULL;

	ReleaseDC(ghwnd, ghdc);
	ghdc = NULL;

	if (gpFile)			// Closing the log file here
	{
		fprintf(gpFile, "Log File Is Successfully Closed.\n");
		fclose(gpFile);
		gpFile = NULL;
	}
}
